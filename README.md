# OpenML dataset: Turkish-Super-League-Matches-(1959-2021)

https://www.openml.org/d/43841

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This data is collected using a Python script and selenium.
Content
This dataset includes all of the matches played in Turkish Super League with information about matchdays, half and full time results, red cards seen by both teams etc.
Racing Bar Chart
https://public.flourish.studio/visualisation/3760773/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43841) of an [OpenML dataset](https://www.openml.org/d/43841). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43841/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43841/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43841/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

